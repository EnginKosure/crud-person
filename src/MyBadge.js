import React, { Component } from 'react';
import Badge from '@material-ui/core/Badge';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    margin: {
        margin: theme.spacing(2),
    },
    padding: {
        padding: theme.spacing(0, 2),
    },
}));

class MyBadge extends Component {

    render() {
        return (
            <div className="person-container">
                <Badge sty={this.style} className={useStyles.margin} badgeContent={5} color="primary">
                </Badge>
            </div>
        );
    }
}
export default MyBadge;