import React, { Component } from 'react';
//import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';

class Person extends Component {
    render() {
        return (
            <div className="person">
                <h2 className="person_name">Name:{this.props.person.name}, Job:{this.props.person.job}</h2>
                {/* <h2 className="person_name">Name: <p id='name'>{this.props.person.name}</p> Job:{this.props.person.job}</h2> */}
                <h3 className="person_extraInfo">{this.props.person.extraInfo}</h3>
                <div className="control-buttons">
                    <button className="edit"
                        onClick={() => this.props.dispatch({ type: 'EDIT_PERSON', id: this.props.person.id })}
                    >Edit</button>
                    <Button variant="contained" color="secondary" className="delete" onClick={() => this.props.dispatch({ type: 'DELETE_PERSON', id: this.props.person.id })}>Delete</Button>
                </div>
            </div>
        )
    }
}

export default connect()(Person);