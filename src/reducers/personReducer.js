const personReducer = (state = [{ id: '111', name: 'John', job: 'Scrum master', extraInfo: 'Beginner', editing: false }, { id: '222', name: 'Frank', job: 'Developer', extraInfo: 'Pro', editing: false }, { id: '333', name: 'Sara', job: 'Tech-lead', extraInfo: 'Elementary', editing: false }], action) => {
    //console.log(state)
    switch (action.type) {
        case 'ADD_PERSON':
            //console.log(state)
            return [...state, action.data];
        case 'DELETE_PERSON':
            return state.filter((person) => person.id !== action.id);
        case 'EDIT_PERSON':
            return state.map((person) => person.id === action.id ? { ...person, editing: !person.editing } : person);
        case 'UPDATE':
            return state.map((person) => {
                if (person.id === action.id) {
                    return {
                        ...person,
                        name: action.data.newName,
                        job: action.data.newJob,
                        extraInfo: action.data.newExtraInfo,
                        editing: !person.editing
                    }
                } else return person
            })
        case 'SORT_PERSON':
            //console.log(state)
            return [...state].sort((person1, person2) => person1.name > person2.name ? 1 : -1);
        case 'SORT_REVERSE':
            //console.log(state)
            return [...state].sort((person1, person2) => person1.name < person2.name ? 1 : -1);
        default:
            return state;
    }

}
export default personReducer;