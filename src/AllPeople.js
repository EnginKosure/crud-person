import React, { Component } from 'react';
import { connect } from 'react-redux';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Person from './Person';
import EditComponent from './EditComponent';

configure({ adapter: new Adapter() });

class AllPeople extends Component {
    handleSort = (e) => {
        e.preventDefault();
        this.props.dispatch({ type: 'SORT_PERSON', name: this.props.people.name })
        this.forceUpdate();
    }
    handleSortReverse = (e) => {
        e.preventDefault();
        this.props.dispatch({ type: 'SORT_REVERSE', name: this.props.people.name })
        this.forceUpdate();
    }
    //  componentDidUpdate() {
    //         this.props.dispatch({ type: 'SORT_PERSON', name: this.props.people.name })
    //     }
    handleEdit = (id, data) => {
        this.props.dispatch({ type: 'UPDATE', id, data })
    }
    render() {
        return (
            <div>
                <h1 className="person_heading">All People</h1>
                {this.props.people.map((person) => (
                    <div key={person.id}>
                        {person.editing ? <EditComponent person={person} key={person.id} onClick={this.handleEdit} /> :
                            <Person key={person.id} person={person} />}
                    </div>
                ))}

                {/* <button className="sort" onClick={() => {
                    this.props.dispatch({ type: 'SORT_PERSON', name: this.props.people.name });
                    this.forceUpdate();
                }
                } >Sort Alphabetically</button> */}
                <div className="button_wrapper" >
                    <button className="sort" onClick={this.handleSort} >Sort Alphabetically</button>
                    <button className="sort" onClick={this.handleSortReverse} >Sort Reverse</button>
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        people: state
    }
}

export default connect(mapStateToProps)(AllPeople);