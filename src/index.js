import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';

import { createStore } from 'redux';
import { Provider } from 'react-redux';//This is necessary to pass the store to the components. Provider component uses React Context to enable us to reach the store from any component, so no need to pass props! If we wrap the parent component, all the children can reach. In this case, <App />. The provider component takes the store as a props.


import personReducer from './reducers/personReducer';

const store = createStore(personReducer);//If you forget to pass the reducer to the store (as an argument), it doesn't render!


ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, document.getElementById('root')
);
