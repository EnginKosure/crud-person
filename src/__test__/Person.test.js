import React from 'react';
import { shallow, mount } from 'enzyme';
import Person from '../Person';

import { sum, double, makeArrayDouble } from "./helpers";


const any = () => <Person />;
describe('Array', function () {
    it('should return -1 when the value is not present', function () {
        mount(<any />)
    });
});

test("The sum of 5 + 4 should return 9", () => {
    expect(sum(5, 4)).toBe(9);
});

test("The double of 22 should return 44", () => {
    expect(double(22)).toBe(44); //MATCHERS
});

test("make array [1,2,5] to return [2,4,10]", () => {
    expect(makeArrayDouble([1, 2, 5])).toEqual([2, 4, 10]);
});
