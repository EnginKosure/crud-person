export const sum = (a, b) => a + b;

export const double = a => a * 2;

export const makeArrayDouble = arr => arr.map(double);