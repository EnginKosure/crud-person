import React, { Component } from 'react';

//import { connect } from 'react-redux';


class EditComponent extends Component {
    handleEdit = (e) => {
        e.preventDefault();
        const newName = this.getName.value;
        const newJob = this.getJob.value;
        const newExtraInfo = this.getExtraInfo.value;
        const data = {
            newName,
            newJob,
            newExtraInfo
        }
        // this.props.dispatch({ type: 'UPDATE', id: this.props.person.id, data: data })
        this.props.onClick(this.props.person.id, data)
    }
    render() {
        return (
            <div key={this.props.person.id} className="person">
                <form className="form" onSubmit={this.handleEdit}>
                    <input required type="text" ref={(input) => this.getName = input}
                        defaultValue={this.props.person.name} placeholder="Enter the name" /><br />
                    <input required type="text" ref={(input) => this.getJob = input}
                        defaultValue={this.props.person.job} placeholder="Job" /> <br />
                    <textarea required rows="2" ref={(input) => this.getExtraInfo = input}
                        defaultValue={this.props.person.extraInfo} cols="20" placeholder="Extra info" /><br />
                    <button>Update</button>
                </form>
            </div>
        )
    }
}

export default EditComponent;