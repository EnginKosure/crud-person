import React, { Component } from 'react';
import { AppBar, Tabs, Tab } from '@material-ui/core';

class Nav extends Component {
    render() {
        return (
            <AppBar title="My App">
                <Tabs>
                    <Tab label="HOME" />
                    <Tab label="ABOUT US" />
                    <Tab label="PAGE 1" />
                    <Tab label="PAGE 2" />
                </Tabs>
            </AppBar>
        )
    }
}

export default Nav;