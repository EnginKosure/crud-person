import React, { Component } from 'react';
import PersonForm from './PersonForm';
import AllPeople from './AllPeople';
import Nav from './Nav';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Nav />
        <div className="bar">
          <h2 className="center">PERSONNEL RECORDS</h2>
          <button className="button">©</button>
        </div>
        <PersonForm />
        <AllPeople />
      </div>
    );
  }
}
export default App;
