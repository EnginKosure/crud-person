import React, { Component } from 'react';
//import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';//connect() returns a function which takes in your current component as an argument and returns a new component with dispatch method as it’s prop. 
//import MyBadge from './MyBadge';

class PersonForm extends Component {
    handleSubmit = (e) => {
        e.preventDefault();
        const name = this.getName.value;
        const job = this.getJob.value;
        const extraInfo = this.getExtraInfo.value;
        const data = {
            id: new Date(),
            name,
            job,
            extraInfo,
            editing: false
        }
        this.props.dispatch({
            type: 'ADD_PERSON',
            data
        });
        this.getName.value = '';
        this.getJob.value = '';
        this.getExtraInfo.value = '';
        //console.log(data);
    }


    render() {
        return (
            <div className="person-container">
                <h1 className="person_heading">Add a Person</h1>
                <form className="form" onSubmit={this.handleSubmit}>
                    <input required type="text" ref={(input) => this.getName = input} placeholder="Enter the Name" /><br />
                    <input required type="text" ref={(input) => this.getJob = input} placeholder="Job" /><br />
                    <textarea required rows="2" cols="20" ref={(input) => this.getExtraInfo = input} placeholder="Extra Info" /><br />
                    <button>Save</button>
                    {/* <MyBadge /> */}
                </form>
            </div>
        );
    }
}
export default connect()(PersonForm);