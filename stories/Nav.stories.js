import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from "@storybook/addon-actions"
import { withKnobs, text } from "@storybook/addon-knobs";
import Nav from "../src/Nav";



const stories = storiesOf('Nav', module)



stories.addDecorator(withKnobs)
    .add('With a click', () => <Nav onClick={action("clicked")} />)