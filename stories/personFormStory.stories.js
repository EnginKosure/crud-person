import React from 'react';
import { Provider } from 'react-redux';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import Badge from "../src/MyBadge";
import Form from "../src/PersonForm";

const stories = storiesOf('Badge', module)

const style = {
    width: 50,
    height: 50,
    backgroundColor: "red"
}
stories.add('Badge', () => <Badge />)
    .add('PersonFormStory', () => <Provider><Form /></Provider>)

