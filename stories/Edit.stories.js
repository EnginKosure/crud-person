import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from "@storybook/addon-actions"
import EditComponent from "../src/EditComponent";

const stories = storiesOf('Edit', module)

let person = {
    name: 'John',
    job: 'Developer',
    extraInfo: '28'
}
stories.add('With a Person', () => <EditComponent person={person} onClick={() => { }} />)
    .add('With a click', () => <EditComponent person={person} onClick={action('clicked')} />)